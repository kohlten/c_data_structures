CFLAGS = -c -Wall -Wextra -Werror -g
CC = gcc
NAME = libdata_structures.a

CFILES = src/hashtable.c src/linked_list.c src/stack.c src/vector_array.c src/queue.c src/dynamic_string.c
OBJ = $(patsubst src/%.c, obj/%.o, $(CFILES))

all:	$(NAME)

$(NAME): build $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "Done"

build:
	-@mkdir obj

obj/%.o: src/%.c
	@$(CC) $(CFLAGS) -c -o $@ $<
	@echo "CC $(CFLAGS) -c -o $@ $<"

clean:
	@rm -rf obj

fclean: clean
	@rm -f $(NAME)

re: fclean all

.PHONY: clean fclean re build