/**
 * @defgroup String
 * @{
 */

#ifndef STRING_H
#define STRING_H

#define STRING_CHUNK_SIZE 20
#define STRING_INIT_CAPACITY STRING_CHUNK_SIZE

typedef struct s_string t_string;

struct s_string {
    char *items;
    unsigned long long capacity;
    unsigned long long total;
};

t_string *string_new(t_string *string);

unsigned long long string_length(t_string *string);

int string_resize(t_string *string, unsigned long long capacity);

int string_add(t_string *string, char item);

int string_cat(t_string *string, char *item);

int string_string_cat(t_string *string, t_string *item);

int string_ncat(t_string *string, char *item, unsigned long long length);

int string_set(t_string *string, unsigned long long index, char item);

char string_get(t_string *string, unsigned long long index);

int string_delete(t_string *string, unsigned long long index);

char *string_get_all(t_string *string);

int string_reset(t_string *string);

void string_free(t_string *string);

#endif

/**
 * @}
 */