//
// Created by kohlten on 3/19/19.
//

#include "queue.h"
#include <strings.h>
#include <stdlib.h>

int init_queue(t_queue *queue) {
    if (!queue)
        return -1;
    bzero(queue, sizeof(t_queue));
    return 0;
}

int push_queue(t_queue *queue, void *data) {
    t_linked_list *list;
    t_linked_list *new;

    if (!queue)
        return -1;
    if (queue->list) {
        list = queue->list;
        new = new_node_linked_list(data, NULL, list);
        if (!new)
            return -1;
        list->prev = new;
        queue->list = new;
    } else {
        queue->list = new_node_linked_list(data, NULL, NULL);
        if (!queue->list)
            return -1;
    }
    queue->nodes++;
    return 0;
}

int pop_queue(t_queue *queue, void **ptr) {
    t_linked_list *list;

    if (!queue)
        return -1;
    if (queue->list) {
        list = queue->list;
        *ptr = list->data;
        if (queue->list->next) {
            queue->list = queue->list->next;
            queue->list->prev = NULL;
        } else
            queue->list = NULL;
        free_node_linked_list(list, false);
        queue->nodes--;
    } else
        return 1;
    return 0;
}

int queue_get_at(t_queue *queue, unsigned long loc, void **ptr) {
    t_linked_list *list;
    unsigned long i;

    if (!queue || !ptr)
        return -1;
    if (!queue->list || loc > (unsigned long)queue->nodes)
        return -1;
    list = queue->list;
    i = 0;
    while (list) {
        if (i == loc) {
            *ptr = list->data;
            return 0;
        }
        list = list->next;
        i++;
    }
    return -1;
}

int in_queue(t_queue *queue, void *data) {
    long i;
    void *node;

    for (i = 0; i < length_queue(queue); i++) {
        if (queue_get_at(queue, i, &node) != 0)
            return -1;
        if (node == data)
            return 1;
    }
    return 0;
}

int remove_queue(t_queue *queue, void *data, bool free_data, void (*free_func)(void *)) {
    t_linked_list *list;

    if (!queue)
        return -1;
    list = queue->list;
    while (list) {
        if (list->data == data) {
            if (free_data) {
                if (free_func)
                    free_func(list->data);
                else
                    free(list->data);
            }
            list->prev = list->next;
            free(list);
            break;
        }
        list = list->next;
    }
    if (list == NULL)
        return -1;
    return 0;
}

long length_queue(t_queue *queue) {
    if (!queue)
        return 0;
    return queue->nodes;
}

int free_queue(t_queue *queue, bool free_data, void (*free_func)(void *)) {
    t_linked_list *list;

    if (!queue)
        return -1;
    list = queue->list;
    while (list) {
        if (free_data) {
            if (free_func)
                free_func(list->data);
            else
                free(list->data);
        }
        list = list->next;
        free_node_linked_list(list->prev, false);
    }
    return 0;
}