#include <stdio.h>
#include <stdlib.h>

#include "vector_array.h"
#include <stdlib.h>

int vector_init(vector *v) {
    v->capacity = VECTOR_INIT_CAPACITY;
    v->total = 0;
    v->items = malloc(sizeof(void *) * v->capacity);
    if (!v->items)
        return -1;
    return 0;
}

int vector_total(vector *v) {
    return v->total;
}

int vector_resize(vector *v, int capacity) {
    void **items;


    items = realloc(v->items, sizeof(void *) * capacity);
    if (!v->items)
        return -1;
    v->items = items;
    v->capacity = capacity;
    return 0;
}

int vector_add(vector *v, void *item) {
    if (v->capacity == v->total) {
        if (vector_resize(v, v->capacity * 2) != 0)
            return -1;
    }
    v->items[v->total++] = item;
    return 0;
}

int vector_set(vector *v, int index, void *item) {
    if (index < 0 || index > v->total)
        return -1;
    v->items[index] = item;
    return 0;
}

void *vector_get(vector *v, int index) {
    if (index >= 0 && index < v->total)
        return v->items[index];
    return NULL;
}

int vector_delete(vector *v, int index) {
    if (index < 0 || index >= v->total)
        return -1;

    v->items[index] = NULL;

    for (int i = index; i < v->total - 1; i++) {
        v->items[i] = v->items[i + 1];
        v->items[i + 1] = NULL;
    }

    v->total--;

    if (v->total > 0 && v->total == v->capacity / 4) {
        if (vector_resize(v, v->capacity / 2) != 0)
            return -1;
    }
    return 0;
}

void vector_free(vector *v) {
    if (v->items)
        free(v->items);
}