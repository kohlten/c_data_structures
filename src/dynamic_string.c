/**
 * @defgroup String
 * @{
 */

#include "dynamic_string.h"

#include <stdlib.h>
#include <string.h>
#include <strings.h>

/**
 * Creates a new string. If you pass in a string, it will put it inside of it.
 * Otherwise it will malloc a new string and return that.
 * @param string Optional previous string to use
 * @return New string
 */
t_string *string_new(t_string *string) {
    if (!string) {
        string = malloc(sizeof(t_string));
        if (!string)
            return NULL;
    }
    string->capacity = STRING_INIT_CAPACITY;
    string->total = 0;
    string->items = malloc(sizeof(char) * string->capacity);
    if (!string->items)
        return NULL;
    bzero(string->items, sizeof(char) * string->capacity);
    return string;
}

/**
 * Returns the current length of the string
 * @param string
 * @return Length of the string
 */
unsigned long long string_length(t_string *string) {
    return string->total;
}

/**
 * Increases the size of the string to capacity.
 * @param string
 * @param capacity
 * @return -1 on failure, 0 on success
 */
int string_resize(t_string *string, unsigned long long capacity) {
    char *items;
    unsigned int i;

    items = malloc(sizeof(char) * capacity);
    if (!items)
        return -1;
    for (i = 0; i < string->total; i++)
        items[i] = string->items[i];
    free(string->items);
    string->items = items;
    string->capacity = capacity;
    return 0;
}

/**
 * Adds a new character to the end of the string
 * @param string
 * @param item
 * @return -1 on failure, 0 on success
 */
int string_add(t_string *string, char item) {
    if (string->capacity == string->total)
        if (string_resize(string, string->capacity + STRING_CHUNK_SIZE) != 0)
            return -1;
    string->items[string->total++] = item;
    return 0;
}

/**
 * Concatenates the new string on to the end of the current string
 * @param string
 * @param item
 * @return -1 on failure, 0 on success
 */
int string_cat(t_string *string, char *item) {
    unsigned long long item_length;
    unsigned long long i;

    item_length = strlen((char *) item);
    for (i = 0; i < item_length; i++) {
        if (string_add(string, item[i]) != 0)
            return -1;
    }
    return 0;
}

/**
 * Concatenates the new string on to the end of the current string
 * @param string
 * @param item
 * @return -1 on failure, 0 on success
 */
int string_string_cat(t_string *string, t_string *item) {
    unsigned long long item_length;
    unsigned long long i;

    if (!string || !item)
        return -1;
    if (!item->items)
        return -1;
    item_length = string_length(item);
    for (i = 0; i < item_length; i++) {
        if (string_add(string, item->items[i]) != 0)
            return -1;
    }
    return 0;
}

/**
 * Concatenates n characters from the item onto the end of the string
 * @param string
 * @param item
 * @param len
 * @return -1 on failure, 0 on success
 */
int string_ncat(t_string *string, char *item, unsigned long long len) {
    unsigned long long i;

    for (i = 0; i < len; i++) {
        if (string_add(string, item[i]) != 0)
            return -1;
    }
    return 0;
}

/**
 * Sets the string location to the item
 * @param string
 * @param index
 * @param item
 * @return -1 on failure, 0 on success
 */
int string_set(t_string *string, unsigned long long index, char item) {
    if (index < string->total) {
        string->items[index] = item;
        return 0;
    }
    return -1;
}

/**
 * Returns the item inside of the string at index
 * @param string
 * @param index
 * @return -1 on failure, the character otherwise
 */
char string_get(t_string *string, unsigned long long index) {
    if (index >= string->total)
        return -1;
    return string->items[index];
}

/**
 * Returns the string currently stored inside of the string
 * @param string
 * @return NULL on failure, the whole string on success. May or may not be null terminated.
 */
char *string_get_all(t_string *string) {
    char *items;

    items = string->items;
    string->items = NULL;
    if (string_reset(string) != 0)
        return NULL;
    return items;
}

/**
 * Deletes the character located in the spot of index
 * @param string
 * @param index
 * @return -1 on failure, 0 on success
 */
int string_delete(t_string *string, unsigned long long index) {
    if (index >= string->total)
        return -1;
    string->items[index] = 0;
    for (unsigned long long i = index; i < string->total - 1; i++) {
        string->items[i] = string->items[i + 1];
        string->items[i + 1] = 0;
    }
    string->total--;
    if (string->total > 0 && string->total == string->capacity / 4) {
        if (string_resize(string, string->capacity - STRING_CHUNK_SIZE) != 0)
            return -1;
    }
    return 0;
}

/**
 * Frees the string and creates a new string
 * @param string
 * @return -1 on failure, 0 on success
 */
int string_reset(t_string *string) {
    string_free(string);
    if (string_new(string) == NULL)
        return -1;
    return 0;
}

/**
 * Frees the string
 * @param string
 */
void string_free(t_string *string) {
    if (string->items)
        free(string->items);
}

/**
 * @}
 */