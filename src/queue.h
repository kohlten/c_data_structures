//
// Created by kohlten on 3/19/19.
//

#ifndef QUEUE_H
#define QUEUE_H

#include "linked_list.h"

typedef struct s_queue t_queue;

struct s_queue {
    t_linked_list *list;
    long nodes;
};

int init_queue(t_queue *queue);
int push_queue(t_queue *queue, void *data);
int pop_queue(t_queue *queue, void **ptr);
int in_queue(t_queue *queue, void *data);
int queue_get_at(t_queue *queue, unsigned long loc, void **ptr);
long length_queue(t_queue *queue);
int remove_queue(t_queue *queue, void *data, bool free_data, void (*free_func)(void *));
int free_queue(t_queue *queue, bool free_data, void (*free_func)(void *));

#endif //QUEUE_H
