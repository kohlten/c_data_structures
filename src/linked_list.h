#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdbool.h>

typedef struct s_linked_list
{
	void *data;
	struct s_linked_list *next;
	struct s_linked_list *prev;
}	t_linked_list;

t_linked_list *new_node_linked_list(void *data, t_linked_list *prev, t_linked_list *next);
void free_node_linked_list(t_linked_list *list, bool free_data);
void free_linked_list(t_linked_list *list, bool free_data);

#endif