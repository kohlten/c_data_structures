#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define HASHTABLE_DEFAULT_SIZE 100

typedef enum
{
	HASH_MALLOC_ERROR   = -1,
	HASH_VALUE_ERROR    = -2,
	HASH_DUPLICATE_DATA = -3,
	HASH_EMPTY_KEY      = -4,
	HASH_NOT_FOUND      = -5,
	HASH_SUCCESS        =  0,
} hashtable_error;

typedef struct s_hashtable_node
{
	void 					*data;
	char 					*key;
	struct s_hashtable_node *next;
	struct s_hashtable_node *prev;
} t_hashtable_node;

typedef struct s_hashtable
{
	t_hashtable_node	**list;
	long				size;
	long 				nodes;
	long 				collisions;
}	t_hashtable;

hashtable_error init_hashtable(t_hashtable *table, long size);
hashtable_error add_node_hashtable(t_hashtable *table, char *key, void *data);
hashtable_error get_node_hashtable(t_hashtable *table, char *key, void **data);
hashtable_error remove_node_hashtable(t_hashtable *table, char *key, bool free_data, void (*free_func)(void *ptr));
hashtable_error get_node_lengths_hashtable(t_hashtable *table, int **ptr);
hashtable_error resize_hashtable(t_hashtable *table, long new_size);
hashtable_error get_items_hashtable(t_hashtable *table, t_hashtable_node ***ptr);
hashtable_error set_node_hashtable(t_hashtable *table, char *key, void *data, int free_data, void (*free_func)(void *));
int node_in_hashtable(t_hashtable *table, char *key);
void free_hashtable(t_hashtable *table, bool free_data, void (*free_func)(void *ptr));

uint32_t murmur3_32(const uint8_t *key, size_t len, uint32_t seed);

#endif