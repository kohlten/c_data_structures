/**
 * @defgroup Hashtable
 * @{
 */

#include "hashtable.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

/**
 * Initalizes the hashtable. If size is less than or equal to zero,
 * the size of the internal array will be HASHTABLE_DEFAULT_SIZE.
 * @param table The hashtable struct
 * @param size Initial size
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error init_hashtable(t_hashtable *table, long size) {
    if (size <= 0)
        size = 1;
    if (!table)
        return HASH_VALUE_ERROR;
    table->size = size;
    table->list = malloc(sizeof(t_hashtable_node *) * (size + 1));
    if (!table->list)
        return HASH_MALLOC_ERROR;
    bzero(table->list, sizeof(t_hashtable_node **) * (size + 1));
    table->nodes = 0;
    table->collisions = 0;
    if (!table->list)
        return HASH_MALLOC_ERROR;
    return HASH_SUCCESS;
}

/**
 * Hashes a string
 * @param key String to hash
 * @param len How long the string is
 * @param seed What seed to use
 * @return Hashed value
 */
uint32_t murmur3_32(const uint8_t *key, size_t len, uint32_t seed) {
    uint32_t h = seed;
    if (len > 3) {
        const u_int32_t *key_x4 = (const uint32_t *) key;
        size_t i = len >> 2;
        do {
            u_int32_t k = *key_x4++;
            k *= 0xcc9e2d51;
            k = (k << 15) | (k >> 17);
            k *= 0x1b873593;
            h ^= k;
            h = (h << 13) | (h >> 19);
            h = (h * 5) + 0xe6546b64;
        } while (--i);
        key = (const uint8_t *) key_x4;
    }
    if (len & 3) {
        size_t i = len & 3;
        u_int32_t k = 0;
        key = &key[i - 1];
        do {
            k <<= 8;
            k |= *key--;
        } while (--i);
        k *= 0xcc9e2d51;
        k = (k << 15) | (k >> 17);
        k *= 0x1b873593;
        h ^= k;
    }
    h ^= len;
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

/**
 * Adds a new node to the hash table using key and data
 * @param table Hashtable struct
 * @param key Key to assign the data to
 * @param data The data to assign to the key
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error add_node_hashtable(t_hashtable *table, char *key, void *data) {
    long long hash;
    t_hashtable_node *node;
    t_hashtable_node *tmp;

    if (!table || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((uint8_t *) key, strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = malloc(sizeof(t_hashtable_node));
    if (!node)
        return HASH_MALLOC_ERROR;
    bzero(node, sizeof(t_hashtable_node));
    node->data = data;
    node->key = strdup(key);
    if (!node->key)
        return HASH_MALLOC_ERROR;
    if (table->list[hash]) {
        tmp = table->list[hash];
        while (tmp->next) {
            if (strcmp(tmp->key, key) == 0) {
                free(node->key);
                free(node);
                return HASH_DUPLICATE_DATA;
            }
            tmp = tmp->next;
        }
        tmp->next = node;
        tmp->next->prev = tmp;
        table->collisions++;
    } else
        table->list[hash] = node;
    table->nodes++;
    if (table->nodes > table->size) {
        return resize_hashtable(table, table->size * 2);
    }
    return HASH_SUCCESS;
}

/**
 * If the table has a node assigned to the key, will assign the new data to that key.
 * If a free function is given, it will free the data with that function if free_data is 1.
 * @param table Hashtable struct
 * @param key Key to assign the new data to
 * @param data Data to assign to the key
 * @param free_data Whether to free the data or not
 * @param free_func Optional function to free the data with. If not supplied, will use the libc free.
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error set_node_hashtable(t_hashtable *table, char *key, void *data, int free_data, void (*free_func)(void *)) {
    long long hash;
    t_hashtable_node *tmp;

    if (!table || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((uint8_t *) key, strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    if (table->list[hash]) {
        tmp = table->list[hash];
        while (tmp) {
            if (strcmp(tmp->key, key) == 0) {
                if (free_data) {
                    if (free_func)
                        free_func(tmp->data);
                    else
                        free(tmp->data);
                }
                tmp->data = data;
                return HASH_SUCCESS;
            }
            tmp = tmp->next;
        }
        return HASH_NOT_FOUND;
    } else
        return HASH_NOT_FOUND;
}

/**
 * If the key is found in the hashtable, data will be set to the data assigned to the key.
 * @param table Hashtable struct
 * @param key Key to get the data from
 * @param data Pointer to a value to place the data into
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error get_node_hashtable(t_hashtable *table, char *key, void **data) {
    long hash;
    t_hashtable_node *node;

    if (!table || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((uint8_t *) key, strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    while (node) {
        if (strcmp(key, node->key) == 0) {
            if (data)
                *data = node->data;
            return HASH_SUCCESS;
        }
        node = node->next;
    }
    return HASH_NOT_FOUND;
}


// @TODO Appears to segfault on resizing after calling this function
/**
 * If the key is in the hashtable, it will remove the key from the hashtable and optionally free it.
 * @param table Hashtable struct
 * @param key Key that the node is assigned to
 * @param free_data Whether to free the data or not
 * @param free_func Optional freeing function
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error remove_node_hashtable(t_hashtable *table, char *key, bool free_data, void (*free_func)(void *ptr)) {
    long hash;
    t_hashtable_node *node;
    t_hashtable_node *prev;
    t_hashtable_node *next;

    if (!table)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((uint8_t *) key, strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    prev = NULL;
    next = NULL;
    while (node) {
        next = node->next;
        if (strcmp(node->key, key) == 0)
            break;
        prev = node;
        node = node->next;
    }
    if (node) {
        if (node == table->list[hash])
            table->list[hash] = next;
        if (prev)
            prev->next = next;
        free(node->key);
        if (free_data) {
            if (free_func)
                free_func(node->data);
            else
                free(node->data);
        }
        free(node);
        return HASH_SUCCESS;
    }
    return HASH_NOT_FOUND;
}

/**
 * Frees the hashtable and all the nodes. If you want to free the data, it will using free_func or libc's free if not supplied.
 * @param table Hashtable struct
 * @param free_data Whether to free the data
 * @param free_func Optional function to use to free the data
 */
void free_hashtable(t_hashtable *table, bool free_data, void (*free_func)(void *ptr)) {
    t_hashtable_node *current;
    t_hashtable_node *next;

    for (int i = 0; i < table->size; i++) {
        if (table->list[i]) {
            current = table->list[i];
            next = current->next;
            while (current) {
                free(current->key);
                if (free_data) {
                    if (free_func)
                        free_func(current->data);
                    else
                        free(current->data);
                }
                free(current);
                current = next;
                if (current)
                    next = current->next;
            }
        }
    }
    free(table->list);
}

/**
 * Get the lengths of the hashtable spots.
 * @param table Hashtable struct
 * @param ptr Where to place the lengths
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error get_node_lengths_hashtable(t_hashtable *table, int **ptr) {
    int *lengths;
    t_hashtable_node *node;

    if (!table)
        return HASH_VALUE_ERROR;
    lengths = malloc(sizeof(int) * table->size);
    if (!lengths)
        return HASH_MALLOC_ERROR;
    bzero(lengths, sizeof(int) * table->size);
    for (int i = 0; i < table->size; i++) {
        node = table->list[i];
        while (node) {
            lengths[i]++;
            node = node->next;
        }
    }
    *ptr = lengths;
    return HASH_SUCCESS;
}

/**
 * Returns all the nodes in the hashtable
 * @param table Hashtable struct
 * @param ptr Where to place the nodes
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error get_items_hashtable(t_hashtable *table, t_hashtable_node ***ptr) {
    t_hashtable_node **items;
    t_hashtable_node *node;
    int pos;

    if (!table)
        return HASH_VALUE_ERROR;
    items = malloc(sizeof(t_hashtable_node *) * table->nodes);
    if (!items)
        return HASH_MALLOC_ERROR;
    bzero(items, sizeof(t_hashtable_node *) * table->nodes);
    pos = 0;
    for (int i = 0; i < table->size; i++) {
        node = table->list[i];
        while (node) {
            items[pos] = node;
            node = node->next;
            pos++;
        }
    }
    *ptr = items;
    return HASH_SUCCESS;
}

/**
 * Resizes the hashtable to the new size
 * @param table Hashtable struct
 * @param new_size New size for the hashtable
 * @return HASH_SUCCESS on success, otherwise error.
 */
hashtable_error resize_hashtable(t_hashtable *table, long new_size) {
    t_hashtable_node **items;
    hashtable_error code;
    long nodes;

    code = get_items_hashtable(table, &items);
    if (code != HASH_SUCCESS)
        return code;
    if (new_size <= 0)
        new_size = table->size * 2;
    free(table->list);
    table->list = malloc(sizeof(t_hashtable_node *) * (new_size + 1));
    if (!table->list)
        return HASH_MALLOC_ERROR;
    bzero(table->list, sizeof(t_hashtable_node *) * (new_size + 1));
    nodes = table->nodes;
    table->nodes = 0;
    table->collisions = 0;
    table->size = new_size;
    for (int i = 0; i < nodes; i++) {
        code = add_node_hashtable(table, items[i]->key, items[i]->data);
        if (code != HASH_SUCCESS)
            return code;
        free(items[i]->key);
        free(items[i]);
    }
    free(items);
    return HASH_SUCCESS;
}

/**
 * Returns if a key is in the hashtable
 * @param table Hashtable struct
 * @param key Key to find
 * @return 1 if the node is in the hashtable, 0 otherwise
 */
int node_in_hashtable(t_hashtable *table, char *key) {
    long hash;
    t_hashtable_node *node;

    if (!table || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((uint8_t *) key, strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    while (node) {
        if (strcmp(key, node->key) == 0)
            return 1;
        node = node->next;
    }
    return 0;
}

/**
 * @}
 */